angular.module('StockPrimate', [
  'ionic',
  'ionic.rating',
  'ion-floating-menu',
  'firebase',
  'angular-cache',
  'ngCordova',
  'nvd3',
  'nvChart',
  'cb.x2js',
  'StockPrimate.controllers',
  'StockPrimate.services',
  'StockPrimate.filters',
  'StockPrimate.directives',
])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      $cordovaStatusbar.overlaysWebView(true);
      //$cordovaStatusBar.style(3); Black, opaque

      $cordovaStatusbar.styleHex('#400040'); //violet

    }
    // if (window.StatusBar && cordova.platformId == 'android') {
    //     StatusBar.backgroundColorByHexString("#400040");
    //   } Status Bar color+ for Android (?)
  });
})


.config(['$sceDelegateProvider', function($sceDelegateProvider) {
         $sceDelegateProvider.resourceUrlWhitelist(['self',
         'http://www.forbes.com/markets/feed/**',
         'http://www.forbes.com/markets/index.xml**',
         'http://mhnystatic.s3.amazonaws.com/**',
         'http://mhnystatic2.s3.amazonaws.com/**',
         'http://finance.yahoo.com/rss/headline?s= + ticker**']);
       }])

//
// .config(['$httpProvider', function ($httpProvider) {
// //Reset headers to avoid OPTIONS request (aka preflight)
//     $httpProvider.defaults.headers.common = {};
//     $httpProvider.defaults.headers.post = {};
//     $httpProvider.defaults.headers.put = {};
//     $httpProvider.defaults.headers.patch = {};
// }])

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url:'/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.tabs', {
    url: '/tab',
    views: {
      'menuContent': {
        templateUrl: 'templates/tabs.html',
        //controller: 'TabsCtrl'
      }
    }
  })

  .state('app.tabs.primate', {
    url: '/primate',
    views: {
      'tab-primate': {
        templateUrl: 'templates/tab-primate.html',
        //controller: 'PrimateCtrl'
      }
    }
  })

  .state('app.tabs.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('app.tabs.portfolio', {
      url: '/portfolio',
      views: {
        'tab-portfolio': {
          templateUrl: 'templates/tab-portfolio.html'
        }
      }
    })

  .state('app.myStocks', {
      url: '/my-stocks',
      views: {
        'menuContent': {
          templateUrl: 'templates/my-stocks.html',
          controller: 'MyStocksCtrl'
        }
      }
    })

    .state('app.stock', {
      url: '/:stockTicker',
      views: {
        'menuContent': {
          templateUrl: 'templates/stock.html',
          controller: 'StockCtrl'
        }
      }
    })

  .state('app.tabs.news', {
    url: '/news',
    views: {
      'tab-news': {
        templateUrl: 'templates/tab-news.html',
        controller: 'JournalCtrl'
      }
    }
  })

  ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/tab/dash');

});
